import React, { Component } from 'react';
import { withRouter} from 'react-router-dom';
import MyContext from '../Context';
import Next from '../assets/images/Next.svg';
import Adventure from '../assets/images/Adventure.svg';
import Drama from '../assets/images/Drama.svg';
import Fiction from '../assets/images/Fiction.svg';
import History from '../assets/images/History.svg';
import Humour from '../assets/images/Humour.svg';
import Philosophy from '../assets/images/Philosophy.svg';
import Politics from '../assets/images/Politics.svg';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import '../Styles/button.scss'
class Button extends Component{
    constructor(props){
        super(props);
        this.state = {
            clickValue: null
        };
        this.handleClick = this.handleClick.bind(this);
      }



         getGenreImage = (genreValue) => {
          switch (genreValue) {
            case 'fiction':
              return Fiction;
            case 'philosophy':
              return Philosophy;
            case 'drama':
              return Drama;
            case 'history':
              return History;
            case 'humuor':
              return Humour;
            case 'adventure':
              return Adventure;
            default:
              return Politics;
          }
        }


    handleClick = (e) => {
        e.preventDefault();
        let a =  this.props.value;
        this.context.state.setQuery(a);
       this.props.history.push(a);
      }

    render(){
        return(
            <Container className='genreCardContainer' tabIndex="1" onClick={this.handleClick}>
            <Row noGutters={true}>
              <Col xs={2}>
                <img className='genreImage' src={this.getGenreImage(this.props.value)} alt={this.props.title} />
              </Col>
              <Col xs={9}>
                {this.props.title}
              </Col>
              <Col xs={1}>
                <img src={Next} alt='Next'/>
              </Col>
            </Row>
          </Container>
        );
    }

}



Button.contextType=MyContext;
export default withRouter(Button);