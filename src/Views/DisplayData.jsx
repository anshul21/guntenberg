import React, {Component} from 'react';
import MyContext from '../Context';
import '../Styles/displayData.scss'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';


class DisplayData extends Component {
  state={
show: false,
  }



      checkFormat(bookFormat){
        var i;
        for(i = 0; i < bookFormat.length; i++ ){
            if(bookFormat[i].search(".htm") !== -1)
                return bookFormat[i];
            else if(bookFormat[i].search(".txt") !== -1)
                return bookFormat[i];
      }
    }

    getMimeType = (formats) => {
      let html = Object.keys(formats).filter(format => formats[format].match(/text\/html/i));
      if (html.length) return formats[html[0]];
      let txt = Object.keys(formats).filter(format => formats[format].match(/^.+\.txt$/i));
      if (txt.length) return formats[txt[0]];
      let pdf = Object.keys(formats).filter(format => formats[format].match(/^.+\.pdf$/i));
      if (pdf.length) return formats[txt[0]];
      return null;
    };

    handleOnClick = (event) => {
      let url = this.getMimeType(this.props.book.formats);
      if(!url){
        alert("No readable format of this book! try another");
      } else {
        window.open(url);
      }
    };

    checkImageSize(imgSize){
        var str;
        if(imgSize.search(".small.") !== -1){
            str = imgSize.replace("large", "small");
            return str;
        }
        else
            return  imgSize;
    }



    render(){
    return (
        <div>
          <Container fluid={true} className="bookCard" onClick={this.handleOnClick}>
            <Row noGutters={true} className="justify-content-md-center">
              <div className="cardImage">
                <img src={this.checkImageSize(this.props.book.formats['image/jpeg'])} alt="img" />
              </div>
            </Row>
            <Row noGutters={true} className="justify-content-md-center">
              <div className="cardtext">
                <p className="bookTitle">{`${this.props.book.title.substring(0, 20)}` || 'Not Provided'}</p>
                <p className="bookAuthor">{(this.props.book.authors[0] && this.props.book.authors[0].name) || 'Not Provided'}</p>
              </div>
            </Row>
          </Container>
        </div>
      )
    };


}
DisplayData.contextType = MyContext;
export default DisplayData;