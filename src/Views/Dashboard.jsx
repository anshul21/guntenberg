import React from 'react';
import Button from './Buton'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import '../Styles/dashboard.scss'
import Jumbotron from 'react-bootstrap/Jumbotron';
class Dashboard extends React.Component {
  constructor() {
    super();
    this.state = {
      types:[
        { title: 'Fiction', value: 'fiction'},
        { title: 'Philosophy', value: 'philosophy'},
        { title: 'Drama', value: 'drama' },
        { title: 'History', value: 'history' },
        { title: 'Humuor', value: 'humuor' },
        { title: 'Adventure', value: 'adventure' },
        { title: 'Politics', value: 'politics' }
      ]
      }
  }

  render() {
  return (
    <div className="app" >
        <Jumbotron fluid className="upper"  >
        <Container>
          <Row className="justify-content-md-center">
            <Col sm={8} xs={12}>
              <h1>Gutenberg Project</h1>
              <p>
                A social cataloging website that allows you to freely search its database of books, annotations,
                and reviews.
              </p>
            </Col>
          </Row>
        </Container>
      </Jumbotron>
<Container>
        <Row className="justify-content-md-center">
          <Col sm={8} xs={12}>
            <Row>
              {
                this.state.types.map(gn => {
                  return(
                    <Col xs={12} sm={6} key={gn.value}>
                      <Button title={gn.title} value={gn.value}/>
                    </Col>
                  )
                })
              }
            </Row>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
}

export default Dashboard;
