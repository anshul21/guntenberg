import React from 'react';
import '../Styles/bookList.scss'
import DisplayData from './DisplayData';
import Search from '../assets/images/Search.svg';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Back from '../assets/images/Back.svg';
import InputGroup from 'react-bootstrap/InputGroup';
import MyContext from '../Context';
import axios from 'axios';
import { DebounceInput } from 'react-debounce-input';
import Spinner from 'react-bootstrap/Spinner';
const API = 'http://gutendex.com/books.json/?mime_type=image';

class BookList extends React.Component {

  state={
    focus:false,
    isLoading:false,
    searhText:''
  }

  fetchData(){
    this.setState({isLoading:true});
    axios.get(API, {
      params: {topic: this.context.state.query}
    })
      .then(res => {this.context.state.setResults(res.data.results)
        this.setState({isLoading:false})})
      .catch(error => console.log(error));
  }

componentDidMount(){
   this.fetchData();
}


   redirectBack = () => {
    this.props.history.goBack();
  };

  inputChange (newInput){
    this.context.state.setLoading(true);

   axios.get(API, {
     params: {topic: this.context.state.query, search: newInput}
   })
     .then(res => this.context.state.setResults(res.data.results),this.context.state.setLoading(false))
 }
  render() {
  return (
    <div>
       <Container fluid={true} className='booksSearchContainer'>
        <Container>
        <Row className="justify-content-md-center">
          <Col md={9} sm={12} xs={12}>
            <Row noGutters={true} className="backButton" onClick={this.redirectBack}>
              <img src={Back} alt="Back" />
              <h2>{this.context.state.query.charAt(0).toUpperCase() + this.context.state.query.slice(1)}</h2>
            </Row>
            <Row noGutters={true} className="inputSection">
                <InputGroup className={['inputGroup', this.state.focus? 'active': '']}>
                  <InputGroup.Prepend className="searchIcon">
                    <InputGroup.Text id="basic-addon1">
                      <img src={Search} alt="search"/>
                    </InputGroup.Text>
                  </InputGroup.Prepend>
                  <DebounceInput
                    minLength={2}
                    debounceTimeout={1000}
                    onFocus={() => this.setState({focus:true})}
                    onBlur={() => this.setState({focus:false})}
                    value={this.state.searhText}
                    onChange={(event) => this.inputChange(event.target.value)}
                    className="inputTextArea form-control"
                    type="text"
                    placeholder="search"
                    aria-label="search"
                    aria-describedby="basic-addon1"/>
                </InputGroup>
                  </Row>
            </Col>
          </Row>
        </Container>
      </Container>

        <Container>
        <Row noGutters={true} className="justify-content-md-center">
          <Col lg={9} md={12} sm={12} xs={12}>
            <Row noGutters={true}>
              {

                this.context.state.results.map(book => {
                  return (
                    <Col xs={4} sm={3} md={2} key={book.id}>
                      <DisplayData book={book}/>
                    </Col>
                  )
                })
              }
            </Row>
          </Col>
        </Row>
      </Container>
      {
        this.state.isLoading ? (
        <Container>
          <Row noGutters={true} className="justify-content-md-center pageSpinner">
            <Spinner animation="border" variant="primary" />
          </Row>
        </Container>) : null
      }
{
!this.context.state.results.length && !this.state.isLoading ? (
          <Container>
            <Row noGutters={true} className="justify-content-md-center noContentSection">
              <Col xs={12} className="noContentCol">
                <h3>no-result-found</h3>
              </Col>
              <Col xs={12} className="noContentCol">
                <h3>No Results Found</h3>
              </Col>
            </Row>
          </Container>) : null
      }

    </div>
  );
}
}
BookList.contextType = MyContext;
export default BookList;