import MyContext from '../Context';
import React from 'react';
class MyProvider extends React.Component {
    constructor() {
        super();
        this.state = {
        results: [],
        error: null,
        query: '',
        loading: false,
        setResults: (newValue) => {
            this.setState({ results: newValue })
          },
          setError: (newValue) => {
            this.setState({ error: newValue })
          },
          setQuery: (newValue) => {
            this.setState({ query: newValue })
          },
          setLoading: (newValue) => {
            this.setState({ loading: newValue })
          }
      }
    }


      render(){
        return (
            <MyContext.Provider
                value={{state: this.state}}>
                {this.props.children}
                </MyContext.Provider>

        )
        }
    }


export default MyProvider;