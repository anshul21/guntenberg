import React from 'react';
import './App.scss';
import {  Switch, Route } from 'react-router-dom';
import Dashboard from './Views/Dashboard'
import BookList from './Views/BookList';
import MyContext from './Context';
class App extends React.Component {

  render() {
  return (
  <Switch>
   <Route exact path="/" component={Dashboard} />
    <Route exact path="/:genreename" component={BookList} />
   </Switch>
  );
}
}
App.contextType=MyContext;
export default App;
